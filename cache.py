import urllib.error
import urllib.request
from robot import Robot

class Cache:
    def __init__(self):
        self.cache = {}
        
    def retrieve(self, url):
        #Descargar documento del argumento url
        if url not in self.cache:
            self.cache[url] = Robot(url = url)

    def show(self, url):
        if url not in self.cache:
            self.retrieve(url)
        print(url)
        print(self.cache[url])
    
    def show_all(self):
        #Mostrar todas las urls que ya se han descargado
        print("\nTodas las URLs:")
        for url in self.cache:
            print(f"{url}\n")
        print("\n")
        
    def content(self, url):
        if url not in self.cache:
            self.retrieve(url)
        return self.cache[url]
    