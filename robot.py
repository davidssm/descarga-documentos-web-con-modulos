import urllib.error
import urllib.request

class Robot:
    def __init__(self, url):
        self.url = url
        self.data = ""
            
    def retrieve(self):
        #Descarga del documento si no lo ha descargado antes
        endpoint = urllib.request.urlopen(self.url)
        self.data = endpoint.read().decode('utf-8')
        return self.data
        
    def show(self):
        #Mostrar contenido descargado y llamara a retrieve
        if self.url is not self.data:
            self.data = self.retrieve()
        else:
            self.data = self.data
        return print(self.data)
                
    def content(self):
        #Devolver contenido descargado si lo tiene 
        self.retrieve()
        return(self.data)
    
