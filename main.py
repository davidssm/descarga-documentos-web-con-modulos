import urllib.request
from robot import Robot
from cache import Cache 


if __name__  == '__main__':
    robot1 = Robot('http://gsyc.urjc.es/')
    robot2 = Robot('https://cursosweb.github.io/')
    print("\n TEST ROBOT1 ")
    robot1.retrieve()
    robot1.show()
    robot1.content()
    print("\n TEST ROBOT2 ")
    robot2.retrieve()
    robot2.show()
    robot2.content()
    print("\n TEST CACHE CLASS")
    cache1 = Cache()
    cache2 = Cache()
    cache1.retrieve('http://gsyc.urjc.es/')
    cache1.retrieve('https://cursosweb.github.io/')
    cache1.show_all()
    cache2.retrieve('https://www.aulavirtual.urjc.es')
    cache2.content('https://www.youtube.com')
    cache2.show_all()
    
    